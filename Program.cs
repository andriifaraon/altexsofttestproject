﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SelfLearning
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter ticket number");
                CheckTicket(Console.ReadLine());
            }
        }
        public static void CheckTicket(string ticket)
        {
            if (int.TryParse(ticket, out int i))
            {
                List<int> numbers = new List<int>();
                ticket.ToList().ForEach(x => numbers.Add(int.Parse(x.ToString())));

                if (numbers.Count < 3 || numbers.Count > 8)
                {
                    Console.WriteLine("Ticket number must contain 4 to 8 digits");
                    return;
                }
                if (numbers.Count % 2 != 0)
                    numbers.Insert(0, 0);

                int half = numbers.Count / 2;
                int sum1 = numbers.Take(half).Sum(x => x);
                int sum2 = numbers.Skip(half).Take(half).Sum(x => x);

                if (sum1 == sum2)
                    Console.WriteLine("Ticket is lucky");
                else
                    Console.WriteLine("Ticket isn`t lucky");
            }
            else
            {
                Console.WriteLine("Ticket number must contain only digits");
            }

        }
    }
}
